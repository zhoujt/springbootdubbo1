package com.jk.mapper;

import com.jk.model.User;
import org.apache.ibatis.annotations.*;

import java.util.List;

@Mapper
public interface UserMapper {
    @Results(id = "userMap",value = {
            @Result(property = "loginName",column = "login_name"),
            @Result(property = "id",column = "id"),
            @Result(property = "name",column = "name"),
            @Result(property = "password",column = "password"),
            @Result(property = "phone",column = "phone")
    })
    @SelectProvider(type = UserProvider.class,method = "selectUserWithCondition")
    List<User> selectAllUser(@Param("user") User user);
    @Insert("insert into sys_user(login_name,name,password,phone) values (#{loginName},#{name},#{password},#{phone})")
    void saveUser(@Param("user") User user);
    @ResultMap("userMap")
    @Select("select * from sys_user where id = #{id}")
    User selectUserById(@Param("id") Integer id);
    @Update("update sys_user set login_name = #{user.loginName},name=#{user.name},password=#{user.password},phone=#{user.phone} where id = #{user.id}")
    void updateUserById(@Param("user") User user);
    @Delete("delete from sys_user where id = #{id}")
    void deleteUserById(@Param("id") Integer id);
}
