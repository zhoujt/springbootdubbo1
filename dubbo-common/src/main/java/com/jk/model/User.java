package com.jk.model;

import lombok.Data;

import java.io.Serializable;

@Data
public class User implements Serializable {
    private Integer id;
    private String loginName;
    private String name;
    private String password;
    private String phone;
}
