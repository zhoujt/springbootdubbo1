package com.jk.service;

import com.jk.model.User;

import java.util.List;

public interface UserService {
    void saveUser(User user);

    List<User> selectAllUser(User user);

    User selectUserById(Integer id);

    void updateUserById(User user);

    void deleteUserById(Integer id);
}
