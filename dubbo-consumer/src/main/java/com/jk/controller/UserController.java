package com.jk.controller;

;
import com.alibaba.dubbo.config.annotation.Reference;
import com.jk.model.User;
import com.jk.service.UserService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

@Controller
@RequestMapping("user")
public class UserController {
    @Reference
    private UserService userService;
    /*    @RequestMapping("findAllUser")
        @ResponseBody
        public HashMap<String,Object> selectAllUser(User user){
            HashMap<String, Object> usermap = new HashMap<>();
            List<User> userlist =  userService.selectAllUser(user);
            usermap.put("rows",userlist);
            return usermap;
        }*/
    @RequestMapping("findAllUserb")
    public String selectAllUserb(Model model,User user ){
        List<User> userlist = userService.selectAllUser(user);
        model.addAttribute("userlist",userlist);
        return "bresourcelist";
    }
    @RequestMapping("addUser")
    public String addUser(User user){
        if(user.getId()==null||"".equals(user.getId())){
            userService.saveUser(user);
        }else{
            userService.updateUserById(user);
        }
        return "redirect:findAllUserb";
    }
    @RequestMapping("findUserById")
    @ResponseBody
    public User findUserById(User user){
        return userService.selectUserById(user.getId());
    }

    @RequestMapping("deleteUserById")
    public String deleteUserById(User user){
        userService.deleteUserById(user.getId());
        return "redirect:findAllUserb";
    }
}
